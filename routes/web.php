<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/list', function () {
    return view('list');
})->name('list');


Route::get('/detail/{uuid}', function () {
    return view('detail');
})->name('detail');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
