## Deployment on Local

1.  Clone project
2.  cd into your project
3.  Install composer dependecies
>  composer install

4.  Install NPM dependecies since the frontend running on Vue.js
>  npm install

5.  Create a copy of your .env file
>  cp .env.example .env

6.  Generate an app encryption key
>  php artisan key:generate

7.  Create your own database `take a note your database should run on mySQL`
8.  In the .env file, add database information to allow Laravel to connect to the database
>  In the .env file fill in the `DB_HOST` `DB_PORT` `DB_DATABASE` `DB_USERNAME` `DB_PASSWORD`

9.  Migrate Database
>  php artisan migrate

10. Run the frontend 
>  npm run watch

11. Run the project
>  php artisan serve --port=8000
Your project should run on `http://127.0.0.1:8000` just change the `--port=` with another available port, if port 8000 is used

## Live Project
Try this url on your browser
> http://fabelio-test-monitor-price.herokuapp.com/

## Price Always Update Every 1 Hour From Submitted
These code to check which product should be updated the price if the product submitted at least one hour 
>       $product    = Product::where('updated_at', '<' , Carbon::now()->subHours(1))->get();
        foreach ($product as $key => $value) {
            $crawler    = Goutte::request('GET', $value->url);            
            $product    = Product::find($value->id);
            $product->current_price = $crawler->filter('.price-wrapper > .price')->first()->text();
            $product->updated_at = Carbon::now();
            $product->save();
        }
        
Run this command to starting the cron job 
> php artisan schedule:run

