<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\DomCrawler\Crawler;
use Goutte;
use KubAT\PhpSimple\HtmlDomParser;
use Illuminate\Support\Str;
use App\Product;
use App\Image;
use Nesk\PuPHPeteer\Puppeteer;
use Nesk\Rialto\Data\JsFunction;

class ProductController extends Controller
{
    public function detail ($uuid) {
        $product    = Product::where('uuid', $uuid)->with('images')->first();

        return $product;
    }

    public function store (Request $request) {
        $this->validate($request, [
            'url' => 'required|url'
        ]);

        $url    = $request->url;
        if (Str::contains($url, 'fabelio.com')) {
            $page   = HtmlDomParser::file_get_html($url);
            $crawler    = Goutte::request('GET', $url);
            $product    = new Product();
    
            if ($product->where('url', $url)->exists() === true) {
                return response()->json([
                    'success' => false,
                    'msg'     => 'Product has been exists!'
                ], 200);
            }
            else {
    
                $product->uuid  = Str::uuid();
                $product->url   = $url;
                $product->name  = $crawler->filter('div .page-title-wrapper h1')->first()->text();
                $product->description   = $crawler->filter('div #description')->first()->text();
                $product->price = $crawler->filter('span .price')->first()->text();
                $product->current_price = $crawler->filter('span .price')->first()->text();
                $product->save();
                
                // $image  = [];
                $image = new Image();
                $image->image   = $crawler->filter('[property="og:image"]')->first()->attr('content');
                $image->product_id  = $product->id;
                $image->save();
                
                return response()->json([
                    'success' => true,
                    'msg'     => 'Successfully add new data!',
                    'uuid'    => $product->uuid      
                ], 200);
            }
        } else {
            return response()->json([
                'success' => false,
                'msg' => 'Url must contain fabelio.com'
            ]);
        }
    }

    public function list () {
        $product    = Product::all();
        $data       = array();
        foreach ($product as $key => $value) {
            $data[$key]['name'] = $value->name;
            $data[$key]['description'] = $value->description;
            $data[$key]['price']    = $value->current_price;
            $data[$key]['last_update'] = date('d-m-Y, H:i:s', \strtotime($value->updated_at));
            $data[$key]['detail']   = url("/detail/{$value->uuid}");
        }
        
        return response($data, 200);
    }

    public function test () {
        // $page   = HtmlDomParser::file_get_html('https://fabelio.com/ip/sofa-2-dudukan-manu.html');
        // return $page->find('span[class=price-wrapper] span[class=price]',0)->innertext;
        $crawler    = Goutte::request('GET', 'https://fabelio.com/ip/sofa-2-dudukan-manu.html');
        dd($crawler->filter('.price-wrapper > .price')->first()->text());        
    }

}
