<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Product;
use Carbon\Carbon;
use Symfony\Component\DomCrawler\Crawler;
use Goutte;

class UpdatePrice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:update-price';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $product    = Product::where('updated_at', '<' , Carbon::now()->subHours(1))->get();
        foreach ($product as $key => $value) {
            $crawler    = Goutte::request('GET', $value->url);            
            $product    = Product::find($value->id);
            $product->current_price = $crawler->filter('.price-wrapper > .price')->first()->text();
            $product->updated_at = Carbon::now();
            $product->save();
        }
    }
}
